#pragma once


#include <Ethernet.h>
#include <ArduinoJson.h>

bool readRequest(EthernetClient& client);
void writeResponse(EthernetClient& client, JsonObject& json);
void rainIRQ();
void wspeedIRQ();
void setup();
void loop();
void calcWeather();
float get_light_level();
float get_battery_level();
float get_wind_speed();
int get_wind_direction();
void printWeather();
JsonObject& prepareResponse(JsonBuffer& jsonBuffer);
void HandleServer();